﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Trocar.LanguageQueryExtensions
{


    public class Program {
    public static void Main()
    {
            //IEnumerable<Func<int, int>> funcs = new List<Func<int, int>> { 
            //    (i) => i + 9, 
            //    (i) => i * i, 
            //    (i) => i - 3 };

            //var result = funcs.ReduceInvoke<int>(5);
            //Console.WriteLine(result);

            IEnumerable<Func<string, string>> funcs = new List<Func<string, string>> {
                (s) => s+" bob",
                (s) => s.Replace("bob","world"),
                (s) => s.Replace("hi","hello") };

            var result = funcs.ReduceInvoke<string>("hi");
            Console.WriteLine(result);
        }
    }
    

}
