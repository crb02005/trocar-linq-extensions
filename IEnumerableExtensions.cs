﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trocar.LanguageQueryExtensions
{
    public static class IEnumerableExtensions
    {
        public static T ReduceInvoke<T>(this IEnumerable<Func<T, T>> funcs, T initialValue) => funcs.Aggregate(initialValue, (acc, next) => next.Invoke(acc));
    }

}
